<?php


class aeb_views_plugin_display_page extends views_plugin_display_page {


  function uses_exposed_form_in_block() {
    if($this->has_path()){
      switch ($this->get_option('exposed_block')) {
        case 0:
          return FALSE;
          break;
        case 1:
          return TRUE;
          break;  
        case 2:
          return FALSE;
          break;
      } 
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $themes = list_themes();
    $theme_default = variable_get('theme_default');
    $regions = array_keys($regions = $this->_get_default_theme_regions());
    $default_region = reset($regions);
    $options['region_to_appear'] = array('default' => $default_region);
    $options['exposed_block']['default'] = 0;
    return $options;
  }


  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    if(isset($form['exposed_block'])){
      $regions = $this->_get_default_theme_regions();
      $form['exposed_block']['#options'] = array(
        1 => 'Yes',
        0 => 'No',
        2 => 'Auto',
        );
      $form['exposed_block']['#default_value'] = $this->get_option('exposed_block');
      $form['region_to_appear'] = array(
      	'#type' => 'select',
      	'#options' => $regions,
      	'#description' => 'Region you want this block to automatically appear in',
      	'#default_value' => $this->get_option('region_to_appear'),
      	);
    }
  }

  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
    $section = $form_state['section'];
    switch ($section) {
      case 'exposed_block':
        $this->set_option($section, $form_state['values'][$section]);
        if($this->get_option($section, $form_state['values'][$section]) == 2){
          $this->set_option('region_to_appear', $form_state['values']['region_to_appear']);
        }
        break;
    }
  }

  function options_summary(&$categories, &$options) {
  	parent::options_summary($categories, $options);
    $value = '';
    switch ($this->get_option('exposed_block')) {
      case 0:
        $value = 'No';
  	    break;
	    case 1:
        $value = 'Yes';
  	    break;
  	  case 2:
  	    $value = 'Auto';
        $regions = $this->_get_default_theme_regions();
        $region = $regions[$this->get_option('region_to_appear')];
        $value = $value . ': ' . $region;
  	    break;
  	}
    if ($this->has_path()) {
      $options['exposed_block'] = array(
        'category' => 'exposed',
        'title' => t('Exposed form in block'),
        'value' => $value,
        'desc' => t('Allow the exposed form to appear in a block instead of the view.'),
      );
    }
  }

  /**
   * Provide the block system with any exposed widget blocks for this display.
   */
  function get_special_blocks() {
    $blocks = array();

    if ($this->uses_exposed_form_in_block()) {
      $delta = '-exp-' . $this->view->name . '-' . $this->display->id;
      $desc = t('Exposed form: @view-@display_id', array('@view' => $this->view->name, '@display_id' => $this->display->id));

      $blocks[$delta] = array(
        'info' => $desc,
        'cache' => DRUPAL_NO_CACHE,
      );
    }

    return $blocks;
  }

  // helper function
  function _get_default_theme_regions() {
    $themes = list_themes();
    $theme_default = variable_get('theme_default');
    $regions = $themes[$theme_default]->info['regions'];
    return $regions;
  }
}